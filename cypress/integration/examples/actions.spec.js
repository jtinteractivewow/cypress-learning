/// <reference types="Cypress" />

context('Actions', () => {
  beforeEach(() => {
    cy.visit('https://www.soundtransit.org/')
  })


  it('.type() - type into a DOM element', () => {
    cy.get('input[placeholder="Enter an address"]')
      .type('fake@email.com')
      .should('have.value', 'fake@email.com')
  })
})
